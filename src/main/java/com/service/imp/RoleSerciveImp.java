package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Role;
import com.repository.RoleRepository;
import com.service.RoleService;

@Service
public class RoleSerciveImp implements RoleService{

	@Autowired 
	RoleRepository roleRepository;
	
	
	public Role saveOrUpdateRole(Role role) {
		// TODO Auto-generated method stub
		return roleRepository.save(role);
	}


	public void deleteRole(Long id) {
		// TODO Auto-generated method stub
		roleRepository.deleteById(id);
		
	}

	public Role findRole(Long id) {
		// TODO Auto-generated method stub
		return roleRepository.findById(id).get();
	}


	public List<Role> findRoles() {
		// TODO Auto-generated method stub
		return roleRepository.findAll();
	}

}
