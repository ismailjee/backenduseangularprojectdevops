package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.User;
import com.repository.UserRepository;
import com.service.UserService;

@Service
public class UserSerciveImp implements UserService{

	@Autowired 
	UserRepository userRepository;

	public User saveOrUpdateUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}


	public void deleteUser(Long id) {
		// TODO Auto-generated method stub
		userRepository.deleteById(id);
		
	}


	public User findUser(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id).get();
	}

	
	public List<User> findUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

}
